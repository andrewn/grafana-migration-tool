#!/usr/bin/env node

"use strict";

const httpClient = require("./lib/http-client");
const ArgumentParser = require("argparse").ArgumentParser;

const targetFolder = 208;

const datasourceMap = {
  "-- Grafana --": "-- Grafana --",
  "Prometheus on prometheus-app-01": "prometheus-app-01-inf-gprd",
  "Prometheus in Azure": "prometheus-01-inf-gprd",
  Production: "influxdb-01-inf-gprd",
  $database: "influxdb-01-inf-gprd",
  "prometheus-app-01-inf-gprd": "prometheus-app-01-inf-gprd",
  "-- Mixed --": "influxdb-01-inf-gprd",
  "InfluxDB Internal": "influxdb-01-inf-gprd"
};

function substituteDatasource(sourceDatasource) {
  if (datasourceMap.hasOwnProperty(sourceDatasource)) {
    return datasourceMap[sourceDatasource];
  }
  throw new Error("Unknown datasource: " + sourceDatasource);
}
function replaceDatasources(json) {
  if (!json) return null;
  if (typeof json !== "object") return null;

  Object.keys(json).forEach(key => {
    let v = json[key];
    if (key === "datasource") {
      if (typeof v === "string") {
        json.datasource = substituteDatasource(v);
      } else if (v === null) {
        json.datasource = "influxdb-01-inf-gprd";
      }
    } else {
      replaceDatasources(v);
    }
  });
}

// Warning: this mutates the input
function transformDashboard(dashboard) {
  replaceDatasources(dashboard);
  dashboard.dashboard.id = null;
  dashboard.dashboard.version = null;
  dashboard.folderId = targetFolder;
  dashboard.overwrite = false;

  return dashboard;
}

async function cmd(args) {
  let source = {
    hostname: "performance.gitlab.net",
    protocol: "https",
    auth: process.env.SOURCE_GRAFANA_TOKEN,
    cookie: process.env.SOURCE_GRAFANA_COOKIE
  };

  let target = {
    hostname: "dashboards.gitlab.net",
    protocol: "https",
    auth: process.env.TARGET_GRAFANA_TOKEN,
    cookie: process.env.TARGET_GRAFANA_COOKIE
  };

  let dashboardResults = await httpClient(source, "/api/search");

  for (let dashboardResult of dashboardResults) {
    if (args.matchTitle) {
      if (!dashboardResult.title.toLowerCase().includes(args.matchTitle.toLowerCase())) {
        continue;
      }
    }

    console.log(`# Converting ${dashboardResult.title}`);

    let dashboard = await httpClient(source, `/api/dashboards/uid/${dashboardResult.uid}`);
    let targetDashboard = transformDashboard(dashboard);

    if (args.dryRun) {
      console.log(JSON.stringify(targetDashboard, null, "  "));
    } else {
      let response = await httpClient(target, "/api/dashboards/db", "POST", targetDashboard);

      console.log(JSON.stringify(response, null, "  "));
    }
  }
}

var parser = new ArgumentParser({
  addHelp: true,
  description: "Dashboard migrator"
});
parser.addArgument(["-d", "--dry-run"], {
  dest: "dryRun",
  action: "storeTrue",
  help: "Dry run only"
});
parser.addArgument("--match-title", {
  dest: "matchTitle",
  help: "Match title"
});

var args = parser.parseArgs();
console.dir(args);

cmd(args).catch(e => {
  console.error(e.stack || e);
  process.exit(1);
});
